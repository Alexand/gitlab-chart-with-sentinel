# Overview

This documentation assumes one already knows how to deploy the GitLab chart and knows how to configure a GitLab Kubernetes Agent.

## Resources

- GKE cluster
- [GitLab chart](https://gitlab.com/gitlab-org/charts/gitlab/-/tree/master/charts)
- [Bitnami Redis Chart](https://github.com/bitnami/charts/tree/master/bitnami/redis) - version 5.0.10
- [Gitlab K8s agent](https://docs.gitlab.com/ee/user/clusters/agent/)
- Helm 3

## Steps

1. Create your k8s cluster
1. Clone this repo
1. Deploy Redis Sentinel with
   
    ```shell
    helm install redis bitnami/redis --values redis-values.yaml
    ```

1. Get the database Redis password

    ```shell
    export REDIS_PASSWORD=$(kubectl get secret --namespace default redis -o jsonpath="{.data.redis-password}" | base64 --decode)
    ```

1. Create a k8s secret with this password to share with GitLab chart

   ```shell
   kubectl create secret generic gitlab-sentinel-token --from-literal=token=$REDIS_PASSWORD
   ```

1. (Optional), connect to redis database and sentinel from a custom pod to check if all is well configured:

    ```shell
    # Deploys and connect to a pod with redis-cli installed

    kubectl run --namespace default redis-client --rm --tty -i --restart='Never' \
    --env REDIS_PASSWORD=$REDIS_PASSWORD \
   --image docker.io/bitnami/redis:6.0.10-debian-10-r1 -- bash
    ```

    ```shell
    redis-cli -h redis -p 6379 -a $REDIS_PASSWORD # Read only operations. Needs password.
    redis-cli -h redis -p 26379 # Sentinel access # Only sentinel commands like: `sentinel masters`. Has no password.
    ```

1. Deploy your GitLab chart

    The below command assumes you've cloned the `gitlab-chart` repo and is running the command from its root folder.

    Take the `kas-chart-upgrade-values.yml` from this documentation repo as a reference to deploy your GitLab instance. Remember to override `YOUR_DOMAIN` and `YOUR_IP` by your own values.

    Install the chart. Replace `../kas-chart-upgrade-values.yaml` by the path to the file you've cloned from this repo.

    ```shell
    helm upgrade --install gitlab . --timeout 600s -f ../kas-chart-upgrade-values.yaml
    ```

1. Provision your GitLab Kubernetes Agent

## Checking the Results

By checking `gitlab-kas` logs, we can see that sentinel master got properly discovered ✅

```shell
λ  gitlab-chart ✗ kubectl logs -n default gitlab-kas-859b7f9db7-tqm2p
{"level":"info","time":"2021-02-02T19:52:17.435Z","msg":"Observability endpoint is up","mod_name":"observability","net_network":"tcp","net_address":"[::]:8151"}
{"level":"info","time":"2021-02-02T19:52:17.435Z","msg":"Listening for agentk connections","net_network":"tcp","net_address":"[::]:8150","is_websocket":true}
redis: 2021/02/02 19:52:34 sentinel.go:641: sentinel: discovered new sentinel="00cbc2f8541318343bd3e4fc8240a5e923b60901" for master="mymaster"
redis: 2021/02/02 19:52:34 sentinel.go:610: sentinel: new master="mymaster" addr="10.24.2.168:6379"
```

Let's also check the database for the expected keys to be created. Run the commands from the optional Step 6 to connect with a `redis-cli` to the database. Then run:

```shell
redis:6379> keys gitlab-kas*
1) "gitlab-kas:agent_limit:\xact\xb0S\xf5\x7f\r\xfb\xd0\x89\x7f\"U\xb8\x92\x16!\xe2C\xe7\x94\xef\x8d-\x00\x19\xb3\x87\xbd\rJ\r:0\x00"
2) "gitlab-kas:agent_limit:\xact\xb0S\xf5\x7f\r\xfb\xd0\x89\x7f\"U\xb8\x92\x16!\xe2C\xe7\x94\xef\x8d-\x00\x19\xb3\x87\xbd\rJ\r:1\x00"
3) "gitlab-kas:agent_tracker:conn_by_agent_id:\x02\x00\x00\x00\x00\x00\x00\x00"
4) "gitlab-kas:agent_tracker:conn_by_project_id:\x02\x00\x00\x00\x00\x00\x00\x00"    
```

